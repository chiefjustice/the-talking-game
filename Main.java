import java.util.ArrayList;
import java.util.Scanner;

import Combat.EnemyStats;
import Combat.Moves.MageMoves.Level4.VenomMissile;
import Exploration.Party;
import Exploration.Region;
import Exploration.World;
import Exploration.Regions.BooshLand;
import Exploration.Regions.EdgeOfMap;
import Exploration.Regions.Farm;
import Exploration.Regions.Fields;
import Exploration.Regions.Forest;
import Exploration.Regions.TowerOfNovice;
import Exploration.Regions.Town;

/**
 * The Main class. Running main runs the program.
 * 
 * @author Warren Gildea
 */
public class Main {
    public static void main(String[] args) {
        // Establish party of players from a list of names.
        System.out.println("Please list the names of your players as single words seperated by spaces.");
        Scanner in = new Scanner(System.in);
        String input = in.nextLine()+" ";
        ArrayList<String> names = new ArrayList<String>();
        while (input.length()>1) {
            names.add(input.substring(0, input.indexOf(" ")));
            input = input.substring(input.indexOf(" ")+1);
        }
        Party party = new Party(in, 10, 5, 5, 5, names.toArray(new String[names.size()]));
        if(party.size()==1) party.gainExperience(2);
        if(party.size()==2) party.gainExperience(1);


        System.out.println("Would you like a tutorial?");
        input = in.nextLine();
        if(input.contains("y")) {
            World tutorialWorld = new World();
            try{tutorialWorld.getLocation().enter(party, in);} catch(Exception e) {}
            while(tutorialWorld.getTutoring()) party.act(tutorialWorld, in);
            try{tutorialWorld.getLocation().exit(party, in);} catch(Exception e) {}
        } else if(input.equals("debug")) {
            System.out.println("Unless you are Warren, I don't think you mean to debug like this.");
            try{party.get(0).learn(new VenomMissile(), 0);} catch(Exception e){}
            party.battle(EnemyStats.lesserArm(), EnemyStats.lesserArm());
        }

        /*
         * Down-right & up-left are also adjacent.
         * Right and left are actually NW and SE, respectively.
         * (Everything is rotated, I think.)
         */
        Region[][] regions = {
            {new Town("Oldport"),  new Forest("+"), new Forest("B+")},
            {new Forest("+"), new Farm(),                new Forest("B"),  new BooshLand()},
            {new Forest("+"), new Forest(),              new Forest("b"),  new Forest("B"),   new Forest("Bh"),   new Forest("bH+")},
            {new EdgeOfMap(),           new Forest(),              new Forest("-"),  new Forest(),                new Forest("hr"),   new Forest("Hr")}, //TODO: hedger land
            {new EdgeOfMap(),           new Fields(),              new Farm("r"),    new Farm("r"),     new Fields("r"),    new Fields("h+"),  new Fields("H+")},
            {new EdgeOfMap(),           new Fields(),              new Farm("r"),    new Town("Talkton"),    new Farm(),                   new Fields(),                new Fields("h+")},
            {new EdgeOfMap(),           new Fields("+"), new Fields("-"),  new Farm("r"),     new Farm("r"),      new Fields(),                new Fields("+")},
            {new EdgeOfMap(),           new EdgeOfMap(),           new Fields("r"),  new Farm("r"),     new Fields("r-"),   new Fields(),                new Fields("+")},
            {new EdgeOfMap(),           new EdgeOfMap(),           new Fields(),               new Fields(),                new Fields()},
            {new EdgeOfMap(),           new EdgeOfMap(),           new TowerOfNovice()}
        };

        // Set up a map.
        World map = new World(5, 3, regions);

        for(int i=0;i<1000;i++) {
            party.act(map, in);
        }
        
        in.close();
    }
}
