package Utilities;

import java.util.ArrayList;

public class Utils {
    /**
     * Capitalizes the first letter of the string of an object.
     * @param string the string to capitalize; If the object is not a String, toString() will be run on it.
     * @return the capitalized string
     */
    public static String capitalize(Object string) {
        return string.toString().substring(0, 1).toUpperCase() + string.toString().substring(1);
    }

    /**
     * Randomizes the order of the ArrayList passed in.
     * @param <e> what kind of object the ArrayList is made of
     * @param toRandomize the ArrayList to randoize
     */
    public static <e> void randomizeOrder(ArrayList<e> toRandomize) {
        ArrayList<e> newOrder = new ArrayList<e>();
        while(toRandomize.size()>0) newOrder.add(toRandomize.remove((int) (Math.random()*toRandomize.size())));
        while(newOrder.size()>0) toRandomize.add(newOrder.remove(0));
    }
}
