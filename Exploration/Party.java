package Exploration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Scanner;

import Combat.Battle;
import Combat.Creature;
import Combat.Enemy;
import Combat.Creature.s;
import Combat.Moves.BasicAttack;
import Combat.Moves.MoveSet.Type;
import Utilities.Utils;

public class Party extends ArrayList<PartyMember> {
    /** the names of all of a party's resources */
    public enum r {food, water, maxWater, coins}
    Hashtable<r, Integer> resources;
    double day;
    ArrayList<Item> items;

    /**
     * Creates a new Party of new mages.
     * @param in how the party should take user input
     * @param food how much food the party starts with
     * @param water how much water the party starts with
     * @param maxWater how many waterskins the party starts with
     * @param coins how many coins the party starts with
     * @param names the names of the party memebers
     */
    public Party(Scanner in, int food, int water, int maxWater, int coins, String... names) {
        super();
        for(String name:names) super.add(new PartyMember(name, 10, 10, Type.mage, in, new BasicAttack("Spark", 1)));
        resources = new Hashtable<r, Integer>();
        resources.put(r.food, food);
        resources.put(r.water, water);
        resources.put(r.maxWater, maxWater);
        resources.put(r.coins, coins);
        day = 0;
        items = new ArrayList<Item>();
    }

    /**
     * The party will battle a group of enemies and award XP.
     * 
     * @param enemies the enemies to fight
     * @return whether or not the party won the battle
     */
    public boolean battle(Creature... enemies) {
        Battle battle = new Battle(enemies);
        for(Creature creature:toCreatures()) battle.add(creature);
        boolean victory = battle.doBattle();
        for(PartyMember member:this) // If we don't find the member we're looking for
            member.health = 0; // they were knocked out in battle.
        for(PartyMember member:this) { // Otherwise, for each member,
            for(Creature creature:battle) // we look for them in the battle
                if(creature.toString().equalsIgnoreCase(member.name))
                    member.health = creature.get(s.health); // and copy over the damage.
            for(Creature creature:battle.getRan()) // We also look for them among those who ran from battle,
                if(creature.toString().equalsIgnoreCase(member.name))
                    member.health = creature.get(s.health); // and copy over that damage.
        }

        if(!victory) { // If the player didn't win,
            for(int i=0;i<super.size();i++) { // we look for allies who were knocked out
                if(super.get(i).getHealth()==0) { // because, without anyone there to help them, they bleed out,
                    remove(i); // and are lost by the party
                    i--;
                }
            }
        } else { // Only gain loot if the battle was won.
            gainExperience(battle.getXP());
            gain(battle.getFood(), r.food);
        }

        if(super.size()==0) {
            System.out.println("You are out of party members. You lose.");
            System.exit(0);
        }

        return victory;
    }

    /**
     * Has the party battle some enemies, with a chance to duplicate each enemy (and the new enemy has a chance to get duplicated as well)
     * @param difficulty the chance to duplicate each enemy
     * @param enemies the enemies to fight, before duplication
     * @return wether or not the party won
     */
    public boolean battle(double difficulty, Enemy... enemies) {
        ArrayList<Enemy> newEnemies = new ArrayList<Enemy>(Arrays.asList(enemies));
        for(int i=0;i<newEnemies.size();i++) {
            if(Math.random()<difficulty) {
                newEnemies.add(new Enemy(newEnemies.get(i)));
            }
        }
        return battle(newEnemies.toArray(new Enemy[newEnemies.size()]));
    }

    /**
     * Allows the player to take an action.
     * @param region the region the party is in
     */
    public void act(World world, Scanner in) {
        Region region = world.getLocation();

        System.out.println("You are in "+region+". You may:");
        System.out.println("-5: Release a party member.");
        System.out.println("-4: Get one of your party members more max HP.");
        System.out.println("-3: View party/move items.");
        System.out.println("-2: Get one of your party members a new move.");
        System.out.println("-1: Travel.");
        for(int i=0;i<region.getActions().size();i++) System.out.println(i+": "+region.getActions().get(i)); // List the possible regional actions.

        boolean validInput = false;
        int index = 0; // The following while loop should set this to another value before finishing.
        while(!validInput) {
            String input = in.nextLine();
            try {
                index = Integer.parseInt(input);
                if(index>=-5 && index<=region.getActions().size()) validInput = true; // The -5 is for the additional default actions.
                else System.out.println("Your number does not correspond to an availible action.");
            } catch(Exception e) {
                System.out.println("Please enter the number next to the action you would like to take.");
            }
        }

        System.out.println();

        if(index==-5) { // release a party member
            System.out.println("Who would you like to release?");
            remove(selectPartyMember(in));
            if(super.size()==0) {
                System.out.println("You are out of party members. You lose.");
                System.exit(0);
            }

        } if(index==-4) { // gain max HP
            System.out.println("Who would you like to gain max HP?");
            try {
                selectPartyMember(in).gainMaxHealth();;
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }

        } if(index==-3) { // view party/manage items
            System.out.println(this);
            System.out.println("Which party member would you like to manage the items of?");
            PartyMember toManage = selectPartyMember(in);
            if(toManage!=null) {
                System.out.println(Utils.capitalize(toManage)+" has the moves:");
                for(int i=0;i<toManage.getMoves().size();i++) {
                    System.out.print(toManage.getMoves().get(i)+"\n");
                }
                System.out.println();

                System.out.println(Utils.capitalize(toManage)+" has the items:");
                for(int i=0;i<toManage.getItems().size();i++) {
                    System.out.print(toManage.getItems().get(i).summary()+"\n");
                }
                System.out.println();

                for(int i=0;i<toManage.getItems().size();i++) { //TODO: improve item management
                    System.out.println("Enter \"take\" if you'd like to take "+toManage+"'s "+toManage.getItems().get(i)+".");
                    if(in.nextLine().equals("take")) {
                        items.add(toManage.getItems().remove(i));
                        i--; // account for array shift
                    }
                }
                for(int i=0;i<items.size();i++) {
                    System.out.println("Enter \"give\" if you'd like to give "+toManage+" "+items.get(i)+".");
                    if(in.nextLine().equals("give")) {
                        toManage.getItems().add(items.remove(i));
                        i--; // account for array shift
                    }
                }
            }

        } if(index==-2) { // learn a move
            System.out.println("Who would you like to learn a new move?");
            PartyMember toLearn = selectPartyMember(in);

            System.out.println("\nYour options are:");
            System.out.println(toLearn.getAvailibleMoves().table(toLearn.getExperience()));
            System.out.println(Utils.capitalize(toLearn)+" has "+toLearn.getExperience()+" XP. What move would you like "+toLearn+" to learn?");
            String input = in.nextLine();
            validInput = false;
            while(!validInput) { // learn the move
                if(input.equalsIgnoreCase("cancel")) {
                    validInput = true;
                } else {
                    try {
                        toLearn.getAvailibleMoves().learn(input, toLearn);
                        validInput = true;
                    } catch(Exception e) {
                        System.out.print(e.getMessage());
                        System.out.println("\nYou may also enter \"cancel\" to cancel learning a move.");
                        input = in.nextLine();
                    }
                }
            }  
        } if(index==-1) { // travel
            try {
                world.travel(this, in);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } else { // take a regional action
            region.doAction(this, index, in);
        }
    }

    /**
     * Allows the user to select a party member.
     * @param in where to take input from
     * @return the selected party member, or null if "cancel" was entered
     */
    public PartyMember selectPartyMember(Scanner in) {
        String name = " ";
            while(!(name.equalsIgnoreCase("cancel"))) {
                name = in.nextLine();
                for(PartyMember member:this) {
                    if(name.equalsIgnoreCase(member.toString())) {
                        return(member);
                    }
                }
                if(!name.equals("cancel")) {
                    System.out.println("Party member not found. Your party consists of:");
                    for(PartyMember member:this) {
                        System.out.println(member);
                    }
                    System.out.println("You may also enter \"cancel\" to cancel.");
                }
            }
        return null;
    }

    /**
     * Randomly distributes XP among the party members.
     * @param toGain how many XP points to distribute
     */
    public void gainExperience(int toGain) {
        for(int unawardedXP=toGain; unawardedXP>=1; unawardedXP--)
            super.get((int) (Math.random()*super.size())).experience++;
        for(PartyMember current:this)
            current.reportXPChange(); // We report the XP change all at once so that we don't get multiple +1XP notifications.
    }
    
    /**
     * Modifies one of the creature's stats, and reports the change.
     * @param gain the amount to gain. If this is negative, the stat will decrease.
     * @param stat which stat to modify
     */
    public void gain(int gain, r stat) {
        if(gain>0) System.out.print(" # You gain "+gain+" "+stat+". ");
        if(gain<0) System.out.print(" # You lose "+(-gain)+" "+stat+". ");
        resources.replace(stat, get(stat)+gain);
        if(get(r.water)>get(r.maxWater)) resources.replace(r.water, get(r.maxWater));
        if(gain!=0) System.out.println(report(stat));
    }

    /**
     * Spends time. If this brings the total over a whole day water and food are consumed.
     * If there is insufficient food or water, party members may be lost.
     * @param daysSpent how many days are spent.
     */
    public void time(double daysSpent, Scanner in) throws Exception {
        int nights = ((int) (day+daysSpent)) - ((int) day);
        if (nights>0) {
            while(get(r.food)<super.size()*nights || get(r.water)<super.size()*nights) {
                System.out.println("You have "+super.size()+" party members but only "+get(r.food)+" food and "+get(r.water)+" water.");
                System.out.println("You must release a party member. Who would you like to release?");
                if(!super.remove(selectPartyMember(in)))
                    throw new Exception("Action cancelled.");
                if(super.size()==0) {
                    System.out.println("You are out of party members. You lose.");
                    System.exit(0);
                }
            }
            gain(-super.size()*nights, r.water);
            gain(-super.size()*nights, r.food);
        }
        day += daysSpent;
        day = Math.round(day * 1000)/1000.0; // account for rounding error
        System.out.println(" # "+Math.round(daysSpent * 1000)/1000.0+" days pass. It is now day "+day+".");
    }

    /**
     * Heals each party member.
     * @param toHeal how much to heal them
     */
    public void heal(int toHeal) {
        for(PartyMember current:this) current.heal(toHeal);
    }

    public void refillWater() {
        System.out.println("You refill your water.");
        gain(resources.get(r.maxWater)-resources.get(r.water), r.water);
    }

    public void gainItem(Item toGain) {
        items.add(toGain);
        System.out.println("You've gained the following item. Use \"-3\" to equip it to someone.");
        System.out.println(toGain.summary());
    }

    /**
     * Spends the cost in coins if the party can afford it.
     * @param cost the number of coins to try to spend
     * @return whether or not the party spent the coins
     */
    public boolean purchase(int cost) {
        if(get(r.coins)>=cost) {
            gain(-cost, r.coins);
            return true;
        } else {
            System.out.println("You can't afford that.");
            return false;
        }
    }

    /**
     * Returns a String reporting the value of each of the listed resources.
     * @param toReport all the resources to report
     * @return the report, as a String
     */
    public String report(r... toReport) {
        String report = "You now have "+get(toReport[0])+" "+toReport[0];
        for(int i=1;i<toReport.length;i++) report+=" and "+get(toReport[i])+" "+toReport[i]; //report the remaining resources
        return report+".";
    }

    /**
     * Returns an ArrayList of all the party members as Creatures, minus those with 0 health.
     * @return the ArrayList
     */
    public Creature[] toCreatures() {
        ArrayList<Creature> creatures = new ArrayList<Creature>();
        for(PartyMember current:this)
            if(current.health!=0)
                creatures.add(current.toCreature());
        return creatures.toArray(new Creature[creatures.size()]);
    }

    public int get(r toGet) {return resources.get(toGet);}
    public double getDay() {return day;}

    @Override
    public boolean add(PartyMember e) {
        System.out.println(Utils.capitalize(e)+" is now a member of your party.");
        return super.add(e);
    }

    @Override
    public boolean remove(Object o) {
        if(super.remove(o)) {
            System.out.println(Utils.capitalize(o)+" is no longer a member of the party.");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public PartyMember remove(int index) {
        System.out.println(Utils.capitalize(super.get(index))+" is no longer a member of the party.");
        return super.remove(index);
    }

    @Override
    public String toString() {
        String string = "Your party members:\n";
        for(PartyMember current:this) // list party members
            string+=String.format("%-20s %3d/%-3dHP %3dXP", current, current.getHealth(), current.getMaxHealth(), current.getExperience())+"\n";
        
        string+="\nYou have:\n"; // list resources
        for(r current:resources.keySet())
            string+=current+": "+get(current)+"\n";
        string+="Items: ";
        for(int i=0;i<items.size();i++) {
            string+=items.get(i)+(i+1<items.size() ? ", " : "\n");
        }
        return string;
    }
}
