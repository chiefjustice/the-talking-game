package Exploration.Items;

import Combat.Battle;
import Combat.Creature;
import Combat.EnemyStats;
import Exploration.Item;

public class BooshBranch extends Item {
    public BooshBranch() {
        super("BooshBranch", "At the start of battle, summon 1 basic arm.");
    }
    
    @Override
    public void battleStart(Creature user, Battle battle) {
        battle.add(EnemyStats.basicArm().toAlly(user));
    }
}
