package Exploration.Items;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Exploration.Item;

public class HedgerShell extends Item {

    public HedgerShell() {
        super("HedgerShell", "At the start of your turn, gain 2 block.");
    }

    @Override
    public void turnStart(Creature user, Battle battle) {
        user.gain(2, s.block);
    }
}
