package Exploration.Items;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Exploration.Item;

public class AmuletOfPreperation extends Item {
    public AmuletOfPreperation() {
        super("AmuletOfPreperation", "At the start of battle, gain 2 charge.");
    }

    @Override
    public void battleStart(Creature user, Battle battle) {
        user.gain(2, s.charge);
    }
}
