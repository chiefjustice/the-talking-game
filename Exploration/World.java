package Exploration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import Exploration.Regions.Tutorial;

public class World {
    ArrayList<ArrayList<Region>> regions;
    boolean tutoring;
    int x;
    int y;

    /**
     * Makes a new World
     * @param regions the rows of the map. The map is hexagonal so each column is to the right and slightly above of the one to the left of it.
     */
    public World(int x, int y, Region[]... regions) {
        this.regions = new ArrayList<ArrayList<Region>>();
        for(Region[] current:regions) this.regions.add(new ArrayList<Region>(Arrays.asList(current)));
        tutoring = false;
        this.x = x;
        this.y = y;
    }

    /**
     * Makes a new tutorial world.
     */
    public World() {
        regions = new ArrayList<ArrayList<Region>>() {{
            add(new ArrayList<Region>() {{
                add(new Tutorial());
            }});
        }};
        tutoring = true;
        x = 0;
        y = 0;
    }

    public Region getLocation() {
        return regions.get(x).get(y);
    }

    /**
     * Asks the party where, among the adjacent regions, it would like to travel, and has them travel there.
     * @param party the party to move
     * @param in where to take input from
     * @throws Exception Sometimes the region they are entering or leaving won't let them leave, and will throw an Exception to be printed for the player.
     */
    public void travel(Party party, Scanner in) throws Exception {
        ArrayList<String> options = new ArrayList<String>();
        System.out.println("You may travel:");
        if(regions.get(x).size()>y+1) { // N
            System.out.println("North      to "+regions.get(x).get(y+1));
            options.add("n");
        } if(regions.size()>x+1 && regions.get(x+1).size()>y+1) { // NE
            System.out.println("North-East to "+regions.get(x+1).get(y+1));
            options.add("ne");
        } if(regions.size()>x+1 && regions.get(x+1).size()>y) { // SE
            System.out.println("South-East to "+regions.get(x+1).get(y));
            options.add("se");
        } if(y>0) { // S
            System.out.println("South      to "+regions.get(x).get(y-1));
            options.add("s");
        } if(y>0 && x>0) { // SW
            System.out.println("South-West to "+regions.get(x-1).get(y-1));
            options.add("sw");
        } if(x>0 && regions.get(x-1).size()>y) { // NW
            System.out.println("North-West to "+regions.get(x-1).get(y));
            options.add("nw");
        }

        String input = in.nextLine();
        while(!options.contains(input) && !input.equals("cancel")) {
            System.out.print("Please input ");
            for(String current:options) System.out.print(current+", ");
            System.out.println("or cancel.");
            input = in.nextLine();
        }
        System.out.println();

        if(!input.equals("cancel")) {
            getLocation().exit(party, in);
        }

        int oldX = x;
        int oldY = y;

        // actually move
        if(input.equals("n")) {
            y++;
        } if(input.equals("ne")) {
            x++;
            y++;
        } if(input.equals("se")) {
            x++;
        } if(input.equals("s")) {
            y--;
        } if(input.equals("sw")) {
            x--;
            y--;
        } if(input.equals("nw")) {
            x--;
        }

        if(!input.equals("cancel")) {
            try {
                getLocation().enter(party, in);
            } catch(Exception e) { // Don't actually travel if the location being travelled to says you can't.
                x = oldX;
                y = oldY;
                throw e;
            } finally {
                party.time(regions.get(oldX).get(oldY).getTravelTime() + getLocation().getTravelTime(), in);
            }
        }
    }

    public boolean getTutoring() {
        return getLocation() instanceof Tutorial && ((Tutorial) getLocation()).getTutoring();
    }
}
