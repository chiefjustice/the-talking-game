package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;

import Combat.EnemyStats;
import Exploration.Party;
import Exploration.Region;
import Exploration.Items.BooshBranch;

public class BooshLand extends Region {

    public BooshLand() {
        super("Boosh Land", 0.4, new ArrayList<>(){{
            add("defeat army");
        }});
    }

    @Override
    public void enter(Party party, Scanner in) throws Exception {
        System.out.println("Guards at the perimeter attack you.");
        if(party.battle(EnemyStats.lesserBoosh(), EnemyStats.basicBoosh())) {
            System.out.println("You make it past them and enter the grounds.");
        } else {
            throw new Exception("The guards keep you out.");
        }
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {
        if(actionIndex==0) {
            try {
                party.time(0.5, in);
                System.out.println("You locate a troop and attack the soldiers.");
                if(party.battle(EnemyStats.lesserBoosh(), EnemyStats.lesserBoosh(), EnemyStats.lesserBoosh(), EnemyStats.lesserBoosh())) {
                    System.out.println("A bunch of arms come out of the ground and attack you.");
                    if(party.battle(EnemyStats.lesserArm(), EnemyStats.lesserArm(), EnemyStats.lesserArm(), EnemyStats.lesserArm(), EnemyStats.basicArm(), EnemyStats.basicArm(), EnemyStats.basicArm(), EnemyStats.basicArm())) {
                        System.out.println("This must be the source of the arms, there is an extremely large boosh here, and an even larger boosh next to it.");
                        if(party.battle(EnemyStats.martialBoosh(), EnemyStats.greaterBoosh())) {
                            System.out.println("You have defeated the boosh army. You take the marshall's magical branch with you.");
                            party.gainItem(new BooshBranch());
                            super.getActions().remove("defeat army");
                        }
                    }
                }
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public void exit(Party party, Scanner in) throws Exception {}
    
}
