package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;

import Combat.EnemyStats;
import Exploration.Party;
import Exploration.PartyMember;
import Exploration.Region;
import Exploration.Party.r;

public class Fields extends Region {

    /**
     * Makes a new Fields.
     * @param modifiers rhfHF accepted
     */
    public Fields(String modifiers) {
        super("fields", 0.3, modifiers, new ArrayList<String>() {{
            add("wander");
            add("rest");
            if(modifiers.contains("r")) add("refill water");
        }});
    }

    /**
     * Default is no modifiers.
     */
    public Fields() {this("");}

    @Override
    public void enter(Party party, Scanner in) throws Exception {
        System.out.println("You enter fields" + (super.river ? " with a river." : "."));
        System.out.print("You ");
        if(!wander(party, in)) throw new Exception("You ran away and were unable to enter the field like you meant to.");
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {
        if(actionIndex == 0) { // wander
            try {
                party.time(0.3, in);
                System.out.print("Wandering through the fields, you ");
                wander(party, in);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex == 1) { // rest
            try {
                party.time(1, in);
                party.heal(10);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex == 2) { // refill water
            party.refillWater();
        }
    }

    /**
     * A encounter table. Should have "You " printed before running.
     * @param party the party wandering
     * @param in the input for encountered creatures
     * @return true unless the party ran from a battle
     */
    private boolean wander(Party party, Scanner in) {
        char result = super.applyModifiers(party, in);
        if(result=='r') {
            return false;
        } else if(result=='e')  {
            return true;
        } else {
            double random = Math.random();
            if(random<0.6) System.out.println("are attacked.");
            if(random<0.1) {
                return party.battle(difficulty, EnemyStats.lesserCnaek());
            } else if(random<0.3) {
                return party.battle(difficulty, EnemyStats.lesserFlʌfbôl());
            } else if(random<0.5) {
                return party.battle(difficulty, EnemyStats.lesserHedger());
            } else if(random<0.6) {
                return party.battle(difficulty, EnemyStats.lesserSpydur());
            } else if(random<0.8) {
                System.out.println("find a berry bush.");
                party.gain(1, r.food);
            } else if(random<0.81) {
                System.out.println("find a friendly flʌfbôl.");
                party.add(new PartyMember(EnemyStats.lesserFlʌfbôl(), in));
            } else if(random<0.82) {
                System.out.println("find a friendly hedger.");
                party.add(new PartyMember(EnemyStats.lesserHedger(), in));
            } else if(random<0.825) {
                System.out.println("find a friendly spydur.");
                party.add(new PartyMember(EnemyStats.lesserSpydur(), in));
            } else {
                System.out.println("don't find anything of note.");
            }
        }
        return true;
    }

    @Override
    public void exit(Party party, Scanner in) throws Exception {}   
}
