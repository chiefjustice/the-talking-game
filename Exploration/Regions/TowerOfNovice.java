package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;

import Combat.EnemyStats;
import Exploration.Party;
import Exploration.Region;
import Exploration.Items.AmuletOfPreperation;
import Exploration.Party.r;

public class TowerOfNovice extends Region {
    boolean open;
    int floor;

    public TowerOfNovice() {
        super("The Tower of the Novice", 0.3,
            new ArrayList<String>() {{
                add("ascend");
                add("rest");
            }});

        open = true;
        floor = 0;
    }

    @Override
    public void enter(Party party, Scanner in) throws Exception{
        if(party.getDay()>10) open=false;
        if(!open) {
            throw new Exception("The doors are closed. You may not enter.");
        } else {
            System.out.println("You enter The Tower of the Novice.");
        }
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {
        if(actionIndex==0) { // ascend
            System.out.print("You ascend to floor "+floor+".\n");
            boolean won = true;
            if(floor==0)  party.battle(EnemyStats.lesserHedger());
            else if(floor==1)  won = party.battle(EnemyStats.lesserFlʌfbôl());
            else if(floor==2)  won = party.battle(EnemyStats.lesserHedger());
            else if(floor==3)  won = party.battle(EnemyStats.lesserBoosh());
            else if(floor==4)  won = party.battle(EnemyStats.lesserCnaek());
            else if(floor==5)  party.refillWater();
            else if(floor==6)  won = party.battle(EnemyStats.lesserSpydur());
            else if(floor==7)  won = party.battle(EnemyStats.basicFlʌfbôl());
            else if(floor==8)  won = party.battle(EnemyStats.basicCnaek());
            else if(floor==9)  won = party.battle(EnemyStats.lesserHedger(), EnemyStats.lesserHedger(), EnemyStats.lesserHedger());
            else if(floor==10)  party.refillWater();
            else if(floor==11) won = party.battle(EnemyStats.basicSpydur());
            else if(floor==12) won = party.battle(EnemyStats.lesserFlʌfbôl(), EnemyStats.lesserFlʌfbôl(), EnemyStats.lesserFlʌfbôl(), EnemyStats.lesserFlʌfbôl());
            else if(floor==13) won = party.battle(EnemyStats.basicBoosh());
            else if(floor==14) won = party.battle(EnemyStats.greaterHedger());
            else if(floor==15) party.refillWater();
            else if(floor==16) won = party.battle(EnemyStats.lesserOerb());
            else if(floor==17) {
                party.refillWater();
                party.gain(30, r.coins);
                party.gainItem(new AmuletOfPreperation());
                open=false;
            } else {
                System.out.println("You are already on the top floor.");
                floor--;
            }
            if(!won) {
                System.out.println("You flee to the lower floor and the stairs dissappear behind you. You may not ascend again.");
                open=false;
                super.getActions().remove("ascend");
            } else  {
                floor++;
            }
        } if(actionIndex==1) { // rest
            try {
                party.time(2, in);
                party.heal(10);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public void exit(Party party, Scanner in) throws Exception {
        open = false;
        System.out.println("The doors of the tower close behind you.");
    }
}
