package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;

import Exploration.Party;
import Exploration.Region;

/**
 * To be put places where there is nothing.
 */
public class EdgeOfMap extends Region {

    public EdgeOfMap() {
        super("nothing", 0, new ArrayList<String>());
    }

    @Override
    public void enter(Party party, Scanner in) throws Exception {
        throw new Exception("You cannot go here. There is no content.");
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {}

    @Override
    public void exit(Party party, Scanner in) throws Exception {}
}
