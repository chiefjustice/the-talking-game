package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;

import Combat.EnemyStats;
import Exploration.Party;
import Exploration.Region;
import Exploration.Party.r;
import Exploration.PartyMember;

public class Forest extends Region {
    public Forest(String modifiers) {
        super("a forest", 0.4, modifiers, new ArrayList<String>() {{
            add("wander");
            add("rest");
            if(modifiers.contains("r")) add("refill water");
        }});
    }

    public Forest() {
        this("");
    }

    @Override
    public void enter(Party party, Scanner in) throws Exception {
        System.out.print("You ");
        if(wander(party, in)) {
            System.out.print("And then you ");
            if(wander(party, in)) {
                System.out.println("You enter woods" + (river ? " with a river." : "."));
                return;
            }
        }
        throw new Exception("You ran away and were unable to enter the forest like you meant to.");
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {
        if(actionIndex == 0) { // wander
            try {
                party.time(0.3, in);
                System.out.print("Wandering through the forest, you ");
                wander(party, in);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex == 1) { // rest
            try {
                party.time(1, in);
                if(Math.random()<0.9) {
                    party.heal(15);
                } else {
                    wander(party, in);
                }
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex == 2) { // refill water
            party.refillWater();
        }
    }

    @Override
    public void exit(Party party, Scanner in) throws Exception {}

    /**
     * A encounter table. Should have "You " printed before running.
     * @param party the party wandering
     * @param in the input for encountered creatures
     * @return true unless the party ran from a battle
     */
    private boolean wander(Party party, Scanner in) {
        char result = super.applyModifiers(party, in);
        if(result=='r') {
            return false;
        } else if(result=='e')  {
            return true;
        } else {
            double random = Math.random();
            if(random<0.6) System.out.println("are attacked.");
            if(random<0.1) {
                return party.battle(difficulty, EnemyStats.basicBoosh());
            } else if(random<0.3) {
                return party.battle(difficulty, EnemyStats.basicFlʌfbôl());
            } else if(random<0.5) {
                return party.battle(difficulty, EnemyStats.basicHedger());
            } else if(random<0.6) {
                return party.battle(difficulty, EnemyStats.lesserSpydur(), EnemyStats.lesserSpydur());
            } else if(random<0.9) {
                System.out.println("find a berry bush.");
                party.gain(1, r.food);
            } else if(random<0.91) {
                System.out.println("find a friendly boosh.");
                party.add(new PartyMember(EnemyStats.lesserBoosh(), in));
            } else if(random<0.92) {
                System.out.println("find a friendly flʌfbôl.");
                party.add(new PartyMember(EnemyStats.lesserFlʌfbôl(), in));
            } else if(random<0.93) {
                System.out.println("find a friendly hedger.");
                party.add(new PartyMember(EnemyStats.lesserHedger(), in));
            } else if(random<0.94) {
                System.out.println("find a friendly spydur.");
                party.add(new PartyMember(EnemyStats.lesserSpydur(), in));
            } else {
                System.out.println("don't find anything of note.");
            }
        }
        return true;
    }
}
