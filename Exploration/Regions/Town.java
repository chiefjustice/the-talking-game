package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;

import Exploration.Party;
import Exploration.Region;
import Exploration.Party.r;

public class Town extends Region {

    public Town(String name) {
        super(name, 0.1,
            new ArrayList<String>() {{
                add("buy food"); // 0
                add("sell food"); // 1
                add("buy waterskin"); // 2
                add("day labor"); // 3
                add("train"); // 4
                add("rest"); // 5
                add("refill water"); // 6
            }});
    }

    @Override
    public void enter(Party party, Scanner in) throws Exception {
        System.out.println("You enter the town of "+super.getName()+".");
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {
        if(actionIndex==0 && party.purchase(1)) { // buy food
            party.gain(2, r.food);
        } if(actionIndex==1) {
            if(party.get(r.food)>=4) {
                party.gain(-4, r.food);
                party.gain(1, r.coins);
            } else {
                System.out.println("You need four food to sell, but you only have "+party.get(r.food)+" food.");
            }
        } if(actionIndex==2 && party.purchase(1)) { // buy waterskin
            party.gain(1, r.maxWater);
        } if(actionIndex==3) { // day labor
            try {
                party.time(1.5, in);
                party.gain(party.size(), r.coins);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex==4 && party.purchase(party.size())) { // train
            try {
                party.time(1, in);
                party.gainExperience(party.size());
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex==5) { // rest
            try {
                party.time(0.5, in);
                party.heal(50);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex==6) { // refill water
            party.refillWater();
        }
    }

    @Override
    public void exit(Party party, Scanner in) throws Exception {}
    
}
