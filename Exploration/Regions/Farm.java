package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;

import Combat.EnemyStats;
import Exploration.Party;
import Exploration.Region;
import Exploration.Party.r;

public class Farm extends Region {
    boolean river;

    public Farm(String modifiers) {
        super("a farm", 0.2, new ArrayList<String>() {{
            add("buy food");
            add("fight pests");
            add("rest");
            if(modifiers.contains("r")) add("refill water");
        }});
        this.river = modifiers.contains("r");
    }

    public Farm() {this("");}

    @Override
    public void enter(Party party, Scanner in) throws Exception {
        System.out.println("You enter a farm" + (river ? " with a river." : "."));
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {
        if(actionIndex==0 && party.purchase(1)) { // buy food
            party.gain(3, r.food);
        } if(actionIndex==1) { // fight pests
            try {
                party.time(0.8, in);
                double random = Math.random();
                if(random<0.15 && party.battle(EnemyStats.lesserFlʌfbôl()))
                    party.gain(1, r.coins);
                else if(random<0.3 && party.battle(EnemyStats.lesserFlʌfbôl()))
                    party.gain(1, r.coins);
                else if(random<0.45 && party.battle(EnemyStats.lesserCnaek()))
                    party.gain(1, r.coins);
                else if(random<0.55 && party.battle(EnemyStats.lesserSpydur()))
                    party.gain(1, r.coins);
                else
                    System.out.println("You don't find any pests.");
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex==2) { // rest
            try {
                party.time(0.8, in);
                party.heal(20);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } if(actionIndex==3) { // refill water
            party.refillWater();
        }
    }

    @Override
    public void exit(Party party, Scanner in) throws Exception {}
    
}
