package Exploration.Regions;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import Combat.EnemyStats;
import Exploration.Party;
import Exploration.Party.r;
import Exploration.PartyMember;
import Exploration.Region;

public class Tutorial extends Region {
    Boolean tutoring;

    public Tutorial() {
        super("the tutorial", 0,
            new ArrayList<String>(){{
                add("first battle");
                add("second battle");
                add("rest");
                add("exit tutorial");
            }});
        tutoring = true;
    }

    @Override
    public void enter(Party party, Scanner in) throws Exception {
        System.out.println("Welcome to the Talking Game! (typing game?)");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("This game is still very much in development, but it is finally in a playable state. Welcome to the demo, I guess.");
        TimeUnit.SECONDS.sleep(2);
        System.out.println("The game is entirely text based. Perhaps, some day, far, far in the future, this will not be the case. For now, I hope you like typing and reading.");
        TimeUnit.SECONDS.sleep(4);
        System.out.println("To start the tutorial, enter 0 to select \"first battle.\"\n");
        TimeUnit.SECONDS.sleep(1);
    }

    @Override
    public void doAction(Party party, int actionIndex, Scanner in) {
        try {
            if(actionIndex==0) { // first battle
                System.out.println("Welcome to combat. Your team versus theirs. This is where most of the gameplay happens. The goal is to reduce all enemies to 0HP.");
                TimeUnit.SECONDS.sleep(3);
                System.out.println("Each member of your party is a mage who knows 1 move, \"Spark,\" which deals 1 damage to 1 creature.");
                TimeUnit.SECONDS.sleep(3);
                System.out.println("When it's their turn, they can use it if you type \"spark \" and then the number next to the enemy you wish to target.");
                TimeUnit.SECONDS.sleep(3);
                System.out.println("Enemies fight back. Watch out.");
                TimeUnit.SECONDS.sleep(2);
                party.battle(EnemyStats.lesserArm());
                TimeUnit.SECONDS.sleep(2);
                System.out.println("Congratulations on your first victory! Most fights give XP, which you can use to learn moves.");
                if(party.size()<=2) {
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println("Since your party is small, you've already been granted some XP. You might want to spend that before advancing so you don't die. (Enter -1.)");
                }
                TimeUnit.SECONDS.sleep(2);
                System.out.println("When you are ready, try out the second battle by entering 1.\n");
            } if(actionIndex==1) { // second battle
                System.out.println("Good luck!");
                TimeUnit.SECONDS.sleep(2);
                party.battle(EnemyStats.lesserArm(), EnemyStats.basicArm());
                TimeUnit.SECONDS.sleep(2);
                System.out.println("You'll notice that you don't automatically recover HP. That can be done by resting. Try that now.\n");
                TimeUnit.SECONDS.sleep(3);
            } if(actionIndex==2) { // rest
                System.out.println("So, you've chosen to rest. This will let your party heal, at the cost of time.");
                TimeUnit.SECONDS.sleep(3);
                System.out.println("The immediate problem with spending time is that you need to feed your party.");
                TimeUnit.SECONDS.sleep(4);
                party.time(1, in);
                for(PartyMember current:party) {
                    current.heal(10);
                }
                TimeUnit.SECONDS.sleep(3);
                System.out.println("Food is dropped by enemies, and you can refill your water in towns and rivvers, but make sure you manage your resources well.");
                TimeUnit.SECONDS.sleep(3);
                System.out.println("When you are ready, exit the tutorial.");
                TimeUnit.SECONDS.sleep(2);
            } if(actionIndex==3) { // end tutorial
                tutoring = false;
            }
        } catch(Exception e) {System.out.println("There is a problem at Tutorial.java");}
    }

    @Override
    public void exit(Party party, Scanner in) throws Exception {
        if(party.get(r.food)<20) {
            System.out.println("Here's your food and water back:");
            party.gain(1, r.food);
            party.gain(1, r.water);
            TimeUnit.SECONDS.sleep(2);
        }
        System.out.println("Good luck!");
        TimeUnit.SECONDS.sleep(2);
    }
    
    public Boolean getTutoring() {return tutoring;}
}
