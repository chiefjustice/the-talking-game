package Exploration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import Combat.Ally;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Enemy;
import Combat.Moves.MoveSet;
import Combat.Moves.MoveSet.Type;
import Combat.Moves.Move;
import Utilities.Utils;

/**
 * One member of a party.
 * Contains a name, health, max health, XP, moves, and some methods.
 */
public class PartyMember {
    String name;
    int health;
    int maxHealth;
    int maxHealthPerUpgrade;
    int experience; // XP
    int previousXP; // XP at the time last reported
    ArrayList<Move> moves;
    MoveSet availibleMoves; // availible moves to learn
    ArrayList<Item> items;
    Scanner in;

    public PartyMember(String name, int health, int maxHealth, Type type, Scanner in, Move... moves) {
        this.name = name;
        this.health = health;
        this.maxHealth = maxHealth;
        this.maxHealthPerUpgrade = maxHealth;
        this.experience = 0;
        this.moves = new ArrayList<Move>(Arrays.asList(moves));
        this.availibleMoves = new MoveSet(type);
        this.items = new ArrayList<Item>();
        this.in = in;
    }

    public PartyMember(Enemy member, Scanner in) {
        this(member.toString(), member.get(s.health), member.get(s.health), Type.other, in, member.getMoves().toArray(new Move[member.getMoves().size()]));
    }

    /**
     * Reports the change in the PartyMembers XP since the last time this method was called.
     */
    public void reportXPChange() {
        if(experience>previousXP) System.out.print(" # "+Utils.capitalize(name)+" gains "+(experience-previousXP)+" XP");
        if(previousXP>experience) System.out.print(" # "+Utils.capitalize(name)+" loses "+(previousXP-experience)+" XP");
        if(previousXP!=experience) System.out.println(" and now has "+experience+" XP.");
        previousXP = experience;
    }

    /**
     * Tries to learn a move, if it has enough XP.
     * @param toLearn the move the learn
     * @param cost the XP cost of the move
     * @throws Exception
     */
    public void learn(Move toLearn, int cost) throws Exception {
        if(experience<cost) {
            throw new Exception("Not enough XP. That move costs "+cost+"XP, but "+name+" only has "+experience+"XP.");
        } else {
            System.out.println();
            experience -= cost;
            moves.add(toLearn);
            reportXPChange();
            System.out.println(" # "+Utils.capitalize(name)+" now knows "+toLearn.getName()+".");
        }
    }

    /**
     * Allows the paying of XP equal to the ally's current max HP to gain it max HP.
     * @throws Exception if the ally doesn't have enough XP
     */
    public void gainMaxHealth() throws Exception {
        if(experience<maxHealth) {
            throw new Exception("Gaining "+maxHealthPerUpgrade+" more max HP costs "+maxHealth+"XP, but "+name+" only has "+experience+"XP.");
        } else {
            experience -= maxHealth;
            reportXPChange();
            maxHealth += maxHealthPerUpgrade;
            System.out.println(" # "+Utils.capitalize(name)+" gains "+maxHealthPerUpgrade+" max HP. "+Utils.capitalize(name)+" now has "+maxHealth+" max HP. ");
        }
    }

    /**
     * Heals the ally, not above its maxHP.
     * @param toHeal how much to heal it
     */
    public void heal(int toHeal) {
        if(Math.min(health+toHeal, maxHealth) != health) {
            System.out.print(" # "+Utils.capitalize(name) + " heals " + (Math.min(health+toHeal, maxHealth)-health) + "HP and now has ");
            health = Math.min(health+toHeal, maxHealth);
            System.out.println(health+"HP.");
        }
    }

    public Creature toCreature() {return new Ally(name, health, in, items, moves.toArray(new Move[moves.size()]));}

    @Override
    public String toString() {return name;}
    public int getExperience() {return experience;}
    public int getHealth() {return health;}
    public int getMaxHealth() {return maxHealth;}
    public ArrayList<Item> getItems() {return items;}
    public ArrayList<Move> getMoves() {return moves;}
    public MoveSet getAvailibleMoves() {return availibleMoves;}
}