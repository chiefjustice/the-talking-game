package Exploration;

import Combat.Battle;
import Combat.Creature;

public abstract class Item {
    String name;
    String description;
    
    public Item(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /** will be exeuted at the start of a creature's turn */
    public void turnStart(Creature user, Battle battle) {}
    /** will be executed at the start of combat */
    public void battleStart(Creature user, Battle battle) {}

    public String summary() {
        return name+": "+description;
    }

    @Override
    public String toString() {
        return name;
    }
}
