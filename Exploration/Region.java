package Exploration;

import java.util.ArrayList;
import java.util.Scanner;

import Combat.EnemyStats;

/**
 * An area in which a party might be.
 */
public abstract class Region {
    String name;
    double travelTime;
    ArrayList<String> actions;
    protected boolean river;
    protected double boosh;
    protected double flʌfbôl;
    protected double hedger;
    protected double difficulty;

    /**
     * Makes a new Region
     * @param name the name of the region
     * @param travelTime the time it takes to enter of exit the region, in days
     * @param modifiers rhHfFbB+, and - are supported
     * @param actions the names of the things a party can do in the region
     */
    public Region(String name, double travelTime, String modifiers, ArrayList<String> actions) {
        this.name = name;
        this.travelTime = travelTime;
        this.actions = actions;
        this.river = modifiers.contains("r");
        flʌfbôl = modifiers.contains("F") ? 0.2 : (modifiers.contains("f") ? 0.1 : 0);
        hedger = modifiers.contains("H") ? 0.2 : (modifiers.contains("h") ? 0.1 : 0);
        boosh = modifiers.contains("G") ? 0.2 : (modifiers.contains("g") ? 0.1 : 0);
        difficulty = modifiers.contains("+") ? 0.2 : (modifiers.contains("-") ? 0.05 : 0.1);
    }

    /**
     * Makes a new Region
     * @param name the name of the region
     * @param travelTime the time it takes to enter of exit the region, in days
     * @param actions the names of the things a party can do in the region
     */
    public Region(String name, double travelTime, ArrayList<String> actions) {
        this.name = name;
        this.travelTime = travelTime;
        this.actions = actions;
    }

    /**
     * Applies modifiers that should happen at the beginning of a "wander" method. Should have "You " printed before running.
     * @param party the party wandering
     * @param in the input for encountered creatures
     * @return 'r' means ran, 'e' means encountered, 'n' means nothing happened
     */
    protected char applyModifiers(Party party, Scanner in) {
        double random = Math.random();
        if(random<hedger) {
            System.out.println("are attacked.");
            random-=hedger;
            return party.battle(EnemyStats.lesserHedger()) ? 'e':'r';
        } else if(random<flʌfbôl) {
            System.out.println("are attacked.");
            random-=flʌfbôl;
            return party.battle(EnemyStats.lesserFlʌfbôl()) ? 'e':'r';
        } else if(random<boosh) {
            System.out.println("are attacked.");
            random-=boosh;
            return party.battle(EnemyStats.lesserFlʌfbôl()) ? 'e':'r';
        }
        return 'n';
    }

    public abstract void enter(Party party, Scanner in) throws Exception;
    public abstract void doAction(Party party, int actionIndex, Scanner in);
    public abstract void exit(Party party, Scanner in) throws Exception;

    @Override
    public String toString() {return name;}
    public ArrayList<String> getActions() {return actions;}
    public double getTravelTime() {return travelTime;}
    public String getName() {return name;}
}
