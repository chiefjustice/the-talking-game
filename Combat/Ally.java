package Combat;
/**
 * The person playing the game will control this.
 * 
 * @author Warren Gildea
 */

import java.util.ArrayList;
import java.util.Scanner;

import Combat.Moves.Move;
import Exploration.Item;

public class Ally extends Creature {
    Scanner in;
    String previousInput;

    /**
     * Makes a new ally.
     * @param name the ally's name
     * @param health the ally's health
     * @param moves the ally's moves
     */
    public Ally(String name, int health, Scanner in, ArrayList<Item> items, Move... moves) {
        super(name, true, health, items, moves);
        this.in = in;
    }

    @Override
    public void act(Battle battle) {
        System.out.println("\nThe battle:\n"+battle+ // display current state of the battle
            "It is "+this+"'s turn. What will "+this+" do?"+
            (previousInput==null ? "" : " (enter to repeat \""+previousInput+"\")")); // do not mention prepeating previous move if their wasn't one
        
        boolean notDone = true;
        while(notDone) {
            String input = in.nextLine();
            if(input.equals("") && previousInput!=null) { // enter to repeat previous action
                input = previousInput+" "; // add space to the end of the string to help catch the final word
            } else {
                previousInput = input;
                input += " ";
            }

            // parse input
            String moveName = input.substring(0, input.indexOf(" "));
            input = input.substring(input.indexOf(" ")+1);
            ArrayList<Creature> targets = new ArrayList<Creature>();
            while (input.length()>1) {
                try {
                    targets.add(battle.get(Integer.parseInt(input.substring(0, input.indexOf(" ")))));
                    input = input.substring(input.indexOf(" ")+1);
                } catch(Exception e) {
                    targets = null;
                    input = "";
                }
            }

            // find the move
            Move move = null;
            for(Move check:super.moves)
                if(check.getName().equalsIgnoreCase(moveName))
                    move = check;
            
            // use move
            System.out.println();
            if(moveName.equalsIgnoreCase("run")) {
                battle.run(this);
                notDone = false;
            } else if(move==null) {
                System.out.println("Move "+moveName+" not found. "+this+"'s moves are:");
                for(Move current:moves) System.out.println(current);
                System.out.println("This creature can also \"run\".");
            } else if(targets==null) {
                System.out.println("Invalid targets. Please put the numbers to the left of your targets seperated by spaces after your move. Your options are:");
                System.out.print(battle);
            } else {
                try {
                    Creature[] targetArray = new Creature[targets.size()];
                    targets.toArray(targetArray);
                    move.use(battle, this, targetArray);
                    notDone = false;
                } catch(Exception error) {
                    System.out.println(error.getMessage());
                }
            }
        }
    }

    @Override
    public void prepareForBattle(Battle battle) {
        previousInput = null;
        super.prepareForBattle(battle);
    }

    @Override
    public boolean checkDeath(Battle battle) {
        boolean dead = super.checkDeath(battle);
        if(dead) in.nextLine();
        return dead;
    }

    public Scanner getIn() {return in;}
}
