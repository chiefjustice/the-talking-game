package Combat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import Combat.Moves.Move;
import Exploration.Item;
import Utilities.Utils;

/**
 * The base for all kinds of creature.
 * Can represent a player, pet, or enemy.
 * 
 * @author Warren Gildea
 * @version (a version number or a date)
 */
public abstract class Creature {
    
    protected String name;
    protected boolean team;
    ArrayList<Move> moves;
    /** the names of all the creatures stats */
    public enum s {health, block, stun, poison, force, power, charge, store0, store1};
    protected Hashtable<s, Integer> stats;
    ArrayList<Item> items;

    /**
     * Creates a new Creature.
     * @param name the naem of the creature
     * @param team which team the creature will be on (true means player)
     * @param health the amount of health the creature starts with
     * @param moves the moves the creature has
     */
    public Creature(String name, boolean team, int health, ArrayList<Item> items, Move... moves) {
        this.name = name;
        this.team = team;
        this.items = items;
        this.moves = new ArrayList<Move>(Arrays.asList(moves));
        stats = new Hashtable<s, Integer>();
        stats.put(s.health, health);
        stats.put(s.block, 0);
        stats.put(s.stun, 0);
        stats.put(s.poison, 0);
        stats.put(s.force, 0);
        stats.put(s.power, 0); 
        stats.put(s.charge, 0);
        stats.put(s.store0, 0);
        stats.put(s.store1, 0);
    }
    
    /**
     * This creature deals damage to the target.
     * @param target the recipient of the damage
     * @param damage the amount of damage to deal
     * @return the amount of unblocked damage the target takes
     */
    public int dealDamage(Creature target, int damage) {
        int toDeal = (damage+stats.get(s.power)) * (1+stats.get(s.store0));
        System.out.println(Utils.capitalize(name) + " deals "+toDeal+" damage to "+target+".");
        return target.takeDamage(toDeal);
    }
    
    /**
     * This is the entirity of a creature's turn.
     * It starts with some status updates and then calls {@code act}.
     * @param battle
     */
    public void takeTurn(Battle battle) {
        System.out.println(); //puts a break between turns for easier reading

        for(Item current:items)
            current.turnStart(this, battle);

        if(stats.get(s.force)>0) {
            gain(-stats.get(s.force), s.health); // it takes damage from force
            gain(-1, s.force); // force ticks down
        }

        gain(-stats.get(s.block), s.block); // creature's block wears off
        gain(-stats.get(s.poison), s.health); // it takes damage from poison
        
        gain(-stats.get(s.store0), s.store0); // store moves down
        gain(stats.get(s.store1), s.store0);
        gain(-stats.get(s.store1), s.store1);

        if(stats.get(s.health)>0) {
            if(stats.get(s.stun)>0) {
                System.out.println(this+" is stunned.");
                gain(-1, s.stun);
                if(this instanceof Ally) {
                    ((Ally) this).getIn().nextLine();
                }
            } else {
                act(battle);
            }
        }
    }
    
    /**
     * This is what the creature will do on its turn.
     * @param battle the battle the creature is in.
     */
    public abstract void act(Battle battle);
    
    /**
     * Causes the creature to take damage, preventing as much of the damage as it can with block.
     * @param damage the amount of damage to take
     * @return the amount of unblocked damage dealt
     */
    public int takeDamage(int damage) {
        int oldHealth = stats.get(s.health);
        if(stats.get(s.block)>=damage) {
            gain(-damage, s.block);
        } else {
            if(stats.get(s.block)>0) System.out.println(Utils.capitalize(name)+" blocks "+stats.get(s.block)+" damage. ");
            gain(stats.get(s.block)-damage, s.health);
            stats.replace(s.block, 0);
        }
        return oldHealth-stats.get(s.health);
    }

    /**
     * Checks if the creature has run out of hit points, and removes it from the battle if it has.
     * @param battle the battle the creature is in.
     * @return whether or not the creature was removed.
     */
    public boolean checkDeath(Battle battle) {
        if(stats.get(s.health)<=0) {
            System.out.println(" * "+Utils.capitalize(name)+" is knocked out.");
            battle.remove(this);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Calls {@code startBattle} on each of the creature's moves so they reset.
     */
    public void prepareForBattle(Battle battle) {
        for(Item current:items)
            current.battleStart(this, battle);
        for(Move current:moves)
            current.battleStart();
    }
    
    /**
     * Modifies one of the creature's stats, and reports the change.
     * @param gain the amount to gain. If this is negative, the stat will decrease.
     * @param stat which stat to modify
     */
    public void gain(int gain, s stat) {
        if(gain>0) System.out.print(" * "+Utils.capitalize(name)+" gains "+gain+" "+stat+". ");
        if(gain<0) System.out.print(" * "+Utils.capitalize(name)+" loses "+(-gain)+" "+stat+". ");
        stats.replace(stat, stats.get(stat)+gain);
        if(gain!=0) System.out.println(report(stat));
    }

    /**
     * Returns a String reporting the value of each of the listed stats.
     * @param toReport all the stats to report
     * @return the report, as a String
     */
    public String report(s... toReport) {
        String report = Utils.capitalize(name)+" now has "+stats.get(toReport[0])+" "+toReport[0];
        for(int i=1;i<toReport.length;i++) report+=" and "+stats.get(toReport[i])+" "+toReport[i]; //report the remaining stats
        return report+".";
    }

    /**
     * Returns a String listing all the creature's stats.
     * @return the String
     */
    public String statBar()
    {
        String summary;
        if(team) summary = String.format("%-8s", "allied");
        else summary = String.format("%-8s", "enemy");
        summary += String.format("%-20s", name);
        for(s check:s.values()) if(stats.get(check)>0) summary+=String.format("%4d %-7s", stats.get(check), check);
        return summary;
    }

    public int get(s toGet) {return stats.get(toGet);}
    public ArrayList<Move> getMoves() {return moves;}
    public boolean getTeam() {return team;}
    @Override
    public String toString() {return name;}
}
