package Combat;

import Combat.Moves.BasicAttack;
import Combat.Moves.EnemyMoves.Constrict;
import Combat.Moves.EnemyMoves.Grow;
import Combat.Moves.EnemyMoves.PowerUp;
import Combat.Moves.EnemyMoves.Rally;
import Combat.Moves.EnemyMoves.ShellTackle;
import Combat.Moves.EnemyMoves.Stare;
import Combat.Moves.EnemyMoves.Swarm;
import Combat.Moves.EnemyMoves.ThornShot;
import Combat.Moves.EnemyMoves.VenomBite;
import Combat.Moves.EnemyMoves.Web;

/**
 * A factory for every kind of enemy.
 */
public class EnemyStats {
    public static Enemy lesserArm()         {return new Enemy("lesser arm",          3,  0, 0, new BasicAttack("Slap", 1));}
    public static Enemy basicArm()          {return new Enemy("basic arm",           5,  0, 0, new BasicAttack("Slap", 2));}
    public static Enemy lesserBoosh()       {return new Enemy("lesser boosh",        8,  1, 0, new ThornShot(4), new Grow(1));}
    public static Enemy lesserCnaek()       {return new Enemy("lesser cnaek",        6,  1, 0, new Stare(1), new Constrict(2));}
    public static Enemy lesserFlʌfbôl()     {return new Enemy("lesser flʌfbôl",      9,  1, 1, new BasicAttack("Bite", 1), new PowerUp(1));}
    public static Enemy lesserHedger()      {return new Enemy("lesser hedger",       7,  1, 1, new ShellTackle(1, 1));}
    public static Enemy greaterArm()        {return new Enemy("greater arm",         7,  1, 0, new BasicAttack("Slap", 3));}
    public static Enemy lesserSpydur()      {return new Enemy("lesser spydur",       5,  2, 0, new VenomBite(1), new Web(1));}
    public static Enemy basicCnaek()        {return new Enemy("basic cnaek",        12,  3, 0, new Stare(2), new Constrict(3));}
    public static Enemy basicFlʌfbôl()      {return new Enemy("basic flʌfbôl",      13,  3, 1, new BasicAttack("Bite", 3), new PowerUp(3));}
    public static Enemy basicBoosh()        {return new Enemy("basic boosh",        14,  4, 0, new ThornShot(6), new Grow(2));}
    public static Enemy basicHedger()       {return new Enemy("basic hedger",       10,  4, 1, new ShellTackle(2, 2));}
    public static Enemy basicSpydur()       {return new Enemy("basic spydur",        8,  4, 0, new VenomBite(2), new Web(2));}
    public static Enemy greaterBoosh()      {return new Enemy("greater boosh",      18,  6, 0, new ThornShot(9), new Grow(3));}
    public static Enemy greaterHedger()     {return new Enemy("greater hedger",     15,  7, 1, new ShellTackle(3, 3));}
    public static Enemy lesserOerb()        {return new Enemy("lesser oerb",        32,  7, 0, new Swarm(1));}
    public static Enemy greaterCnaek()      {return new Enemy("greater cnaek",      23,  8, 0, new Stare(5), new Constrict(4));}
    public static Enemy greaterFlʌfbôl()    {return new Enemy("greater flʌfbôl",    25,  8, 1, new BasicAttack("Bite", 5), new PowerUp(5));}
    public static Enemy greaterSpydur()     {return new Enemy("greater spydur",     15,  8, 0, new VenomBite(3), new Web(3));}
    public static Enemy basicOerb()         {return new Enemy("basic oerb",         60, 10, 0, new Swarm(2));}
    public static Enemy martialBoosh()      {return new Enemy("martial boosh",      35, 12, 0, new ThornShot(10), new Grow(3), new Rally(1, 2), new Rally(2, 3));}
    public static Enemy greaterOerb()       {return new Enemy("greater oerb",      100, 20, 0, new Swarm(3));}
}