package Combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import Combat.Moves.Move;
import Combat.Moves.EnemyMoves.EnemyMove;
import Exploration.Item;

/**
 * The base for an enemy.
 * All enemies stats can be defined with the constructor.
 * If you are looking to make an enemy, look at EnemyStats.
 * 
 * @author Warren Gildea
 */
public class Enemy extends Creature
{
    int experience;
    int food;
    int moveIndex;
    ArrayList<Creature> targets;
    
    public Enemy(String name, int health, int experience, int food, ArrayList<Item> items, EnemyMove... moves) {
        super(name, false, health, items, moves);
        this.experience = experience;
        this.food = food;
        moveIndex = 0;
        targets = new ArrayList<Creature>();
        this.moves = new ArrayList<Move>(Arrays.asList(moves));
    }

    public Enemy(String name, int health, int experience, int food, EnemyMove... moves) {
        this(name, health, experience, food, new ArrayList<Item>(), moves);
    }

    /**
     * Creates a copy of an enemy. This is useful to prevent having two variables which point the the same enemy and interact with eachother.
     * @param enemy the enemy to copy; It has none of the original enemy's items.
     */
    public Enemy(Enemy enemy) {
        this(enemy.toString(), enemy.get(s.health), enemy.getXP(), enemy.getFood(), new ArrayList<Item>(), enemy.getMoves().toArray(new EnemyMove[enemy.getMoves().size()]));
    }
    
    @Override
    public void takeTurn(Battle battle) {
        super.takeTurn(battle);
    } 
    
    /**
     * Enemies all loop through their list of moves in order.
     */
    public void act(Battle battle) {
        boolean done = false;
        while(!done) {
            if(moveIndex>=moves.size()) moveIndex=0; // reset to beginning of move list after getting all the way through it
            EnemyMove move = (EnemyMove) moves.get(moveIndex);

            // remove targets which are no longer in the battle
            for(int i=0;i<targets.size();i++) {
                if(!battle.contains(targets.get(i))) {
                    targets.remove(targets.get(i));
                    i--;
                }
            }
            
            // add targets if there aren't enough
            while(move.getTargetCount()>targets.size()) {
                targets.add(battle.randomTarget(!super.team));
            }

            try {
                move.use(battle, this, targets.subList(0, move.getTargetCount()).toArray(new Creature[0])); // only send as many targets as are needed
                done = true;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            moveIndex++;
        }
    }

    /**
     * returns an allied version of the enemy
     * @param in the input from which to command the creature
     * @return an ally with the dame stats
     */
    public Ally toAlly(Scanner in) {return new Ally(name, super.get(s.health), in, items, super.moves.toArray(new Move[0]));}

    /**
     * returns a creature allied to the paramater
     * @param ally the creature on whom's team the new creature should be
     * @return the new creature
     */
    public Creature toAlly(Creature ally) {
        return ally instanceof Ally ? 
            toAlly(((Ally) ally).getIn()) :
            this;
    }

    public ArrayList<Creature> getTargets() {return targets;}
    public int getXP() {return experience;}
    public int getFood() {return food;}

    public void setTarget(Creature target) {targets.set(0, target);}
}
