package Combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import Utilities.Utils;

import java.lang.Math;

/**
 * Contains a list of creatures and some methods to interact with them.
 * 
 * @author Warren Gildea
 */
public class Battle extends ArrayList<Creature>
{
    int experience;
    int food;
    int currentTurn;
    ArrayList<Creature> ran;

    public Battle(Creature... creatures) {
        super(Arrays.asList(creatures));
    }
    
    /**
     * Does the battle.
     * Creatures take turns until one team has been eliminated.
     * @return whether or not the player won the battle
     */
    public boolean doBattle() {
        ran = new ArrayList<Creature>();
        experience = 0;
        for(Creature current:this) {
            current.prepareForBattle(this);
            if(current instanceof Enemy) {
                experience += ((Enemy) current).getXP();
                food += ((Enemy) current).getFood();
            }
        }

        Utils.randomizeOrder(this);
        boolean keepGoing = true;
        while(keepGoing) {
            for(currentTurn=0;currentTurn<super.size();currentTurn++) {
                super.get(currentTurn).takeTurn(this);
                try{TimeUnit.MILLISECONDS.sleep(400);} catch(Exception e){}
                for(int j=0;j<super.size();j++) {
                    if(super.get(j).checkDeath(this)) { // remove dead creatures
                        if(currentTurn>=j) currentTurn--; // acount for the shift in the ArrayList after a creature is removed
                        j--;
                    }
                }
                if(battleOver()) {
                    keepGoing = false;
                    break;
                }
            }
        }

        for(Creature current:this) // check to see if any allies lived
            if(current.getTeam()==true)
                return true;
        
        return false; // if none did, return false
    }
    
    /**
     * Checks if the battle is over, and reports whether or not it is.
     * @return whether or not the battle is over
     */
    public boolean battleOver() {
        boolean alliesLiving = false;
        boolean enemiesLiving = false;
        for(Creature check:this) {
            if(check.getTeam()) alliesLiving = true;
            else enemiesLiving = true;
        }
        if(!enemiesLiving) {
            System.out.println("\nYou win!");
            return true;
        } if(!alliesLiving) {
        System.out.println("\nYou have no more fighters.");
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Has a creature run from the battle.
     * @param running the creature which is running
     */
    public void run(Creature running) {
        if(super.remove(running)) ran.add(running);
    }

    /**
     * Returns a random target from the given team.
     * @param team the pool from which to randomly draw the target; True means allied with the player.
     * @return the target
     */
    public Creature randomTarget(boolean team) {
        Creature target = super.get((int) Math.random()*super.size());
        while(target.getTeam()!=team) target = super.get((int) (Math.random()*super.size()));
        return target;
    }

    /**
     * Adds new creatures to the battle, and puts them last in line to take their turn.
     * @param creatures the creatures to add
     */
    public void addToBattle(Creature... creatures) {
        for(Creature current:creatures) {
            super.add(currentTurn, current);
            currentTurn++; // The whole array shifts, and so then must whose turn it is.
        }
    }

    /**
     * Returns how much XP the enemies originally in the battle are worth.
     * @return how much XP the enemies are worth
     */
    public int getXP() {return experience;}
    public int getFood() {return food;}
    public ArrayList<Creature> getRan() {return ran;}

    @Override
    public String toString() {
        String string = "";
        for(int i=0;i<super.size();i++) string+=i+" "+super.get(i).statBar()+"\n";
        return string;
    }
}
