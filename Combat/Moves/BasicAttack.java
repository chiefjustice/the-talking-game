package Combat.Moves;

import Combat.Battle;
import Combat.Creature;
import Combat.Moves.EnemyMoves.EnemyMove;

public class BasicAttack extends EnemyMove {
    int damage;

    public BasicAttack(String name, int damage) {
        super(name, "Deal "+damage+" damage to 1 creature.", 1);
        this.damage = damage;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) {
        user.dealDamage(targets[0], damage);
    }
}
