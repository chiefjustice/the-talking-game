package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Enemy;
import Combat.EnemyStats;
import Combat.Creature.s;

/**
 * Summons an arm and gains 1 stun.
 * Not to be confused with the mage move.
 */
public class Grow extends EnemyMove {
    int level;

    /**
     * Creates a new Grow move.
     * @param level arm level; 1 is lesser, 2 is basic, 3 is greater
     */
    public Grow(int level) {
        super("Grow", "Summon 1 " + 
                (level==1 ? "lesser arm" :
                (level==2 ? "basic arm" :
                "greater arm")) + 
            ". Gain 1 stun.", 0);
        this.level = level;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        Enemy toSummon =
            level==1 ? EnemyStats.lesserArm() :
            (level==2 ? EnemyStats.basicArm() :
            EnemyStats.greaterArm());
        battle.addToBattle(toSummon.toAlly(user));
        user.gain(1, s.stun);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==0;
    }
    
}
