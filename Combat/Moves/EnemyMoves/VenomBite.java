package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;

public class VenomBite extends EnemyMove {

    int damage;

    public VenomBite(int damage) {
        super("VenomBite", "Deal "+damage+" damage to 1 creature. If it takes any unblocked damage, apply 1 poison to it.", 1);
        this.damage = damage;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        targets[0].gain((user.dealDamage(targets[0], damage)>0?1:0), s.poison);
    }
}
