package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;

public class Web extends EnemyMove {
    int usesPerBattle;
    int uses;

    public Web(int uses) {
        super("Web", "("+uses+" use"+((uses>1)?"s":"")+" per battle) Apply 1 stun to 1 creature.", 1);
        usesPerBattle = uses;
    }

    @Override
    public void battleStart() {
        uses = usesPerBattle;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            targets[0].gain(1, s.stun);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }
}
