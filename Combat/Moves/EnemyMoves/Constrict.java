package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;

public class Constrict extends EnemyMove {
    int force;

    public Constrict(int force) {
        super("Constrict", "Apply "+force+" force to 1 creature.", 1);
        this.force = force;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        targets[0].gain(force, s.force);
    }
}
