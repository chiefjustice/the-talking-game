package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;

public class ThornShot extends EnemyMove {
    int damage;

    public ThornShot(int damage) {
        super("ThornShot", "Deal "+damage+" damage to 1 creature. Gain 1 stun.", 1);
        this.damage = damage;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) {
        user.dealDamage(targets[0], damage);
        user.gain(1, s.stun);
    }
}
