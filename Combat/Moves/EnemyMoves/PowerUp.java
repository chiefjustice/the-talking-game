package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;

public class PowerUp extends EnemyMove {
    int power;

    public PowerUp(int power) {
        super("PowerUp", "Gain "+power+" power.", 0);
        this.power = power;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        user.gain(power, s.power);
    }   
}
