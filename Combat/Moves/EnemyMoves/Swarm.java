package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Enemy;
import Combat.Creature.s;

public class Swarm extends EnemyMove {
    int damage;

    public Swarm(int damage) {
        super("Swarm", "Deal "+damage+" damage to 1 creature. Lose half your health (rounded down) and summon an ally with the same moves as much health as you lost this was.", 1);
        this.damage = damage;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        user.dealDamage(targets[0], damage);
        int lostHealth = user.get(s.health)/2;
        user.gain(-lostHealth, s.health);
        battle.addToBattle(new Enemy(user.toString(), lostHealth, 0, 0, user.getMoves().toArray(new EnemyMove[0])));
    }
    
}
