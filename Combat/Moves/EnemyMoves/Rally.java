package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Enemy;
import Combat.EnemyStats;
import Combat.Creature.s;

public class Rally extends EnemyMove {
    int level;
    int stun;

    public Rally(int level, int stun) {
        super((level==2?"Greater":"")+"Rally",
            "Summon 1 "+(level==1?"lesser boosh":"basic boosh")+". Gain "+stun+" stun.",
            0);
        this.level = level;
        this.stun = stun;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        Enemy toSummon =
            level==1 ? EnemyStats.lesserBoosh() :
            (level==2 ? EnemyStats.basicBoosh() :
            EnemyStats.greaterBoosh()); // level 3's description is not implemented
        battle.addToBattle(toSummon.toAlly(user));
        user.gain(stun, s.stun);
    }
}
