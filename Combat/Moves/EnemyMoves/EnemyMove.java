package Combat.Moves.EnemyMoves;

import Combat.Moves.Move;

public abstract class EnemyMove extends Move {
    int targetCount;

    /**
     * Creates a new EnemyMove
     * @param name the name of the move
     * @param description the description of the move
     * @param targetCount the number of targets the move has; negative means it targets allies
     */
    public EnemyMove(String name, String description, int targetCount) {
        super(name, description);
        this.targetCount = targetCount;
    }
    
    @Override
    public boolean validTargetCount(int count) {
        return count==Math.abs(targetCount);
    }

    public int getTargetCount() {return targetCount;}
}
