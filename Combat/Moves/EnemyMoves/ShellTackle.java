package Combat.Moves.EnemyMoves;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;

public class ShellTackle extends EnemyMove {
    int damage;
    int block;

    public ShellTackle(int damage, int block) {
        super("ShellTackle", "Deal "+damage+" damage to 1 creature and gain "+block+" block.", 1);
        this.damage = damage;
        this.block = block;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) {
        user.dealDamage(targets[0], damage);
        user.gain(block, s.block);
    }
}
