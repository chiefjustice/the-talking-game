package Combat.Moves.EnemyMoves;

import java.util.ArrayList;

import Combat.Battle;
import Combat.Creature;
import Combat.Moves.Move;
import Utilities.Utils;

public class Stare extends EnemyMove {
    int damage;

    public Stare(int damage) {
        super("Stare", "Deal "+damage+" damage to 1 creature. They lose a random one of their moves this combat.", 1);
        this.damage = damage;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        user.dealDamage(targets[0], damage);
        ArrayList<Move> moves = targets[0].getMoves();
        Move toLose = moves.get((int) (Math.random()*moves.size()));
        System.out.println(" * "+Utils.capitalize(targets[0])+" forgets "+toLose.getName()+" this battle.");
        moves.remove(toLose);
    }
}
