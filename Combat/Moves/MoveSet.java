package Combat.Moves;

import java.util.ArrayList;

import Combat.Moves.MageMoves.Level1.Charge;
import Combat.Moves.MageMoves.Level1.Hold;
import Combat.Moves.MageMoves.Level1.Poison;
import Combat.Moves.MageMoves.Level1.Scare;
import Combat.Moves.MageMoves.Level1.Taunt;
import Combat.Moves.MageMoves.Level2.Block;
import Combat.Moves.MageMoves.Level2.Burst;
import Combat.Moves.MageMoves.Level2.Fear;
import Combat.Moves.MageMoves.Level2.Shock;
import Combat.Moves.MageMoves.Level3.Boost;
import Combat.Moves.MageMoves.Level3.Grow;
import Combat.Moves.MageMoves.Level3.Lightning;
import Combat.Moves.MageMoves.Level3.Store;
import Combat.Moves.MageMoves.Level3.Stun;
import Combat.Moves.MageMoves.Level4.Daze;
import Combat.Moves.MageMoves.Level4.DramaticEntrance;
import Combat.Moves.MageMoves.Level4.Grasp;
import Combat.Moves.MageMoves.Level4.Prepare;
import Combat.Moves.MageMoves.Level4.VenomMissile;
import Combat.Moves.MageMoves.Level5.ElridgeGrasp;
import Combat.Moves.MageMoves.Level5.ForceField;
import Combat.Moves.MageMoves.Level5.Freeze;
import Combat.Moves.MageMoves.Level5.Fright;
import Combat.Moves.MageMoves.Level5.ToxicCloud;
import Combat.Moves.MageMoves.Level6.Catalyze;
import Combat.Moves.MageMoves.Level6.GreaterShock;
import Combat.Moves.MageMoves.Level6.Startle;
import Exploration.PartyMember;

/** contains 5 moves from each level */
public class MoveSet {
    ArrayList<ArrayList<Move>> moves;
    int maxXP;
    public enum Type {mage, other};

    public MoveSet(Type type) {
        moves = new ArrayList<ArrayList<Move>>(11);
        
        if(type==Type.mage) {
            addLevel(new BasicAttack("Spark", 1)); //0
            addLevel(new Charge(), new Hold(), new Poison(), new Scare(), new BasicAttack("Spurt", 2), new Taunt()); //1
            addLevel(new Block(), new Fear(), new Shock(), new BasicAttack("Burn", 3), new Burst()); //2
            addLevel(new Boost(), new Grow(), new Lightning(), new Store(), new Stun()); //3
            addLevel(new Daze(), new DramaticEntrance(), new BasicAttack("Fireball", 4), new Grasp(), new Prepare(), new VenomMissile()); //4
            addLevel(new ElridgeGrasp(), new ForceField(), new Freeze(), new Fright(), new ToxicCloud()); //5
            addLevel(new Catalyze(), new GreaterShock(), new Startle(), new BasicAttack("SuperFireball", 8)); //6
            addLevel(); //7
            addLevel(new BasicAttack("Incinerate", 15)); //8
            addLevel(); //9
            addLevel(new BasicAttack("ColumnOfFlame", 100)); //10
        } else {
        }

        for(ArrayList<Move> current:moves) {
            while(current.size()>5) {
                current.remove((int) (Math.random()*current.size()));
            }
        }

        maxXP = 0;
    }

    private void addLevel(Move... moves) {
        this.moves.add(new ArrayList<Move>() {{
            for(Move current:moves) 
                add(current);
        }});
    }

    /**
     * Returns all availible moves in a list.
     * @param currentXP the amount of XP the party member has
     * @return the moves in a list, sorted by level
     */
    public String table(int currentXP) {
        if(currentXP>maxXP) maxXP=currentXP;

        String table = "";
        
        for(int level=0;cost(level)<=maxXP;level++) {
            table+="\nLevel "+level+": ("+cost(level)+"XP)\n";
            for(Move move:moves.get(level))
                table+=move+"\n";
        }

        return table;
    }

    /**
     * Tries to teach a party member a new move.
     * @param moveName the name of the move trying to be learned
     * @param learner the party member trying to learn the move
     * @throws Exception The move might not be found.
     */
    public void learn(String moveName, PartyMember learner) throws Exception {
        for(int l=0;l<moves.size();l++) {
            for(Move move:moves.get(l)) {
                if(move.getName().equalsIgnoreCase(moveName)) {
                    learner.learn(move, MoveSet.cost(l));
                    moves.get(l).remove(move);
                    return;
                }
            }
        }
        throw new Exception("Move not found. The availible moves are:\n"+table(learner.getExperience()));
    }

    /**
     * returns the cost of the level
     * @param level the level to get the cost of
     * @return the cost, in XP, of moves at that level
     */
    public static int cost(int level) {
        if(level==0) return 0;
        if(level==1) return 1;
        if(level==2) return 2;
        if(level==3) return 3;
        if(level==4) return 4;
        if(level==5) return 5;
        if(level==6) return 10;
        if(level==7) return 15;
        if(level==8) return 25;
        if(level==9) return 50;
        if(level==10) return 100;
        System.out.println("There is a problem in MageMoveSet.cost");
        return 0;
    }
}
