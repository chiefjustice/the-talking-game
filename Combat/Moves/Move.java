package Combat.Moves;

import Combat.Battle;
import Combat.Creature;
import Utilities.Utils;

public abstract class Move {

    String name;
    String description;

    /**
     * Gets called by child classes to set name and description.
     * @param name the name of the spell - should have no spaces and be capitalized
     * @param description the description of the spell - written as a complete sentence like a command
     */
    public Move(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Uses the move.
     * @param user the creature using the move
     * @param targets the targets to use the move on
     * @return whether or not the move was used successfully
     */
    public void use(Battle battle, Creature user, Creature... targets) throws Exception {
        if(!validTargetCount(targets.length)) throw new Exception("Invalid number of targets for selected move:\n"+this);

        // ensure all the targets are different
        for(int i=0;i<targets.length;i++) {
            for(int j=i+1;j<targets.length;j++) { // We don't need to check backwards in the list. That would be redundant.
                if(targets[i].equals(targets[j])) {
                    throw new Exception("Duplicate target. All targets must be different creatures.");
                }
            }
        }

        // say the move was used and on who
        System.out.print(Utils.capitalize(user)+" uses "+name);
        if(targets.length>=1) {
            System.out.print(" on "+targets[0]);
            for(int i=1;i<targets.length;i++) {
                System.out.print(", ");
                if(i+1==targets.length) System.out.print("and "); // adds and before final item
                System.out.print(targets[i]);
            }
        }
        System.out.println(".");

        affect(battle, user, targets); // the move actually happens
    }

    /**
     * The move will do what it does.
     * @param user the Creature using the move
     * @param targets the Creatures the move is being used on
     * @throws Exception any errors due to invalid targets, to be printed for the player so they can try another move or target
     */
    public abstract void affect(Battle battle, Creature user, Creature... targets) throws Exception;

    /**
     * Returns whether or not the move works on the given number of targets.
     * @param count how many targets to test
     * @return whether or not that is a valid target count
     */
    public abstract boolean validTargetCount(int count);

    /**
     * If the move has anything it needs to reset each battle, it should do so here.
     */
    public void battleStart() {}

    public String getName() {return name;}
    public String toString() {return name+": "+description;}
}
