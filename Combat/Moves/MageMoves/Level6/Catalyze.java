package Combat.Moves.MageMoves.Level6;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Catalyze extends Move {
    int uses;

    public Catalyze() {
        super("Catalyze", "(2 uses per battle) Apply poison to 1 creature equal to its poison.");
    }

    @Override
    public void battleStart() {
        uses=2;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>0) {
            targets[0].gain(targets[0].get(s.poison), s.poison);
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
    
}
