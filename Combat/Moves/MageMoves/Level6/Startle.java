package Combat.Moves.MageMoves.Level6;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Startle extends Move {
    int uses;

    public Startle() {
        super("Startle", "(1 use per battle) Make up to 5 creatures lose 5HP.");
    }

    @Override
    public void battleStart() {
        uses = 1;
    }
    
    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            for(Creature current:targets) current.gain(-5, s.health);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=5;
    }
}
