package Combat.Moves.MageMoves.Level4;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Grasp extends Move {
    int uses;

    public Grasp() {
        super("Grasp", "(2 uses per battle) Apply 3 force to 1 creature.");
    }

    @Override
    public void battleStart() {
        uses=2;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>0) {
            targets[0].gain(3, s.force);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
}
