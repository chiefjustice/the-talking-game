package Combat.Moves.MageMoves.Level4;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class VenomMissile extends Move {

    public VenomMissile() {
        super("VenomMissile", "Deal 2 damage to 1 creature. If it takes any unblocked damage, apply 1 poison to it.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        targets[0].gain(user.dealDamage(targets[0], 2)>0?1:0, s.poison);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
    
}
