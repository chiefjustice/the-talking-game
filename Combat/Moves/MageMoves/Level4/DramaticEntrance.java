package Combat.Moves.MageMoves.Level4;

import Combat.Battle;
import Combat.Creature;
import Combat.Moves.Move;

public class DramaticEntrance extends Move {
    int uses;

    public DramaticEntrance() {
        super("DramaticEntrance", "(1 use per battle) Deal 4 damage to up to 4 creatures.");
    }

    @Override
    public void battleStart() {
        uses = 1;
    }
    
    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            for(Creature current:targets) user.dealDamage(current, 4);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=4;
    }
}
