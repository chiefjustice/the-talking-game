package Combat.Moves.MageMoves.Level4;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Daze extends Move {
    int uses;

    public Daze() {
        super("Daze", "(1 use per battle) Apply 1 stun to up to 2 creatures.");
    }

    @Override
    public void battleStart() {
        uses = 1;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            for(Creature current:targets) current.gain(1, s.stun);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
            
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=2;
    }
}
