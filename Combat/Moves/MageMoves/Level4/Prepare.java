package Combat.Moves.MageMoves.Level4;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Prepare extends Move {
    int uses;

    public Prepare() {
        super("Prepare", "(1 use per battle) Gain 2 charge.");
    }

    @Override
    public void battleStart() {
        uses = 1;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            user.gain(2, s.charge);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
            
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==0;
    }
}