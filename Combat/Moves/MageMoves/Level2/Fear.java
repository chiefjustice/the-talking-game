package Combat.Moves.MageMoves.Level2;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Fear extends Move {
    public Fear() {
        super("Fear", "Make up to 3 creatures lose 2HP.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        for(Creature target:targets) target.gain(-2, s.health);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=3;
    }
}