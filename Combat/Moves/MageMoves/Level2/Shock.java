package Combat.Moves.MageMoves.Level2;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Shock extends Move {
    public Shock() {
        super("Shock", "(costs 1 charge) Deal 6 damage to 1 creature.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(user.get(s.charge)>=1) {
            user.dealDamage(targets[0], 6);
            user.gain(-1, s.charge);
        } else {
            throw new Exception("Not enough charge.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
}
