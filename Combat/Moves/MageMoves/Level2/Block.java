package Combat.Moves.MageMoves.Level2;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Block extends Move {
    int uses;

    public Block() {
        super("Block", "(3 uses per battle) Gain 8 block.");
    }

    @Override
    public void battleStart() {
        uses = 3;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            user.gain(8, s.block);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==0;
    }
}
