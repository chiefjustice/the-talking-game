package Combat.Moves.MageMoves.Level2;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Burst extends Move {

    public Burst() {
        super("Burst", "Deal 4 damage to 1 target. Gain 1 stun.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        user.dealDamage(targets[0], 4);
        user.gain(1, s.stun);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
    
}
