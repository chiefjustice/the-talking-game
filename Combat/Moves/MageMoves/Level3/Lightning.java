package Combat.Moves.MageMoves.Level3;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Lightning extends Move {
    public Lightning() {
        super("Lightning", "(costs 2 charge) Deal 12 damage to 1 creature.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(user.get(s.charge)>=2) {
            user.dealDamage(targets[0], 12);
            user.gain(-2, s.charge);
        } else {
            throw new Exception("Not enough charge.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
    
}
