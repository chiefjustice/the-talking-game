package Combat.Moves.MageMoves.Level3;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Boost extends Move {
    public Boost() {
        super("Boost", "Apply 1 charge to up to 2 creatures.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        for(Creature current:targets) {
            current.gain(1, s.charge);
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=2;
    }
}
