package Combat.Moves.MageMoves.Level3;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Store extends Move {
    public Store() {
        super("Store", "During your next turn, your moves deal twice as much damage.");
    }
    
    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        user.gain(1, s.store1);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==0;
    }
}
