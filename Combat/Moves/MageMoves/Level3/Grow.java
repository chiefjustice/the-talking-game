package Combat.Moves.MageMoves.Level3;

import Combat.Battle;
import Combat.Creature;
import Combat.EnemyStats;
import Combat.Moves.Move;

public class Grow extends Move {
    int uses;

    public Grow() {
        super("Grow", "(1 use per battle) Summon 1 Lesser Arm.");
    }
    
    @Override
    public void battleStart() {
        uses = 1;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            battle.addToBattle(EnemyStats.lesserArm().toAlly(user));
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==0;
    }

    
}
