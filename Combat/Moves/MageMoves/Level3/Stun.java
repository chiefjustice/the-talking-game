package Combat.Moves.MageMoves.Level3;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Stun extends Move {
    int uses;

    public Stun() {
        super("Stun", "(1 use per battle) Apply 1 stun to 1 creature.");
    }

    @Override
    public void battleStart() {
        uses = 1;
    }
    
    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            targets[0].gain(1, s.stun);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
}
