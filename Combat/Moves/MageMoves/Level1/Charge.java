package Combat.Moves.MageMoves.Level1;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Charge extends Move {
    public Charge() {
        super("Charge", "Gain 1 charge.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        user.gain(1, s.charge);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==0;
    }
}
