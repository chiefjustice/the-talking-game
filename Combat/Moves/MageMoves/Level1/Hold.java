package Combat.Moves.MageMoves.Level1;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Hold extends Move {
    public Hold() {
        super("Hold", "Apply 1 force to 1 creature.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        targets[0].gain(1, s.force);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
    
}
