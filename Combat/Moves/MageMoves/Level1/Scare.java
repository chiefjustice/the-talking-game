package Combat.Moves.MageMoves.Level1;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Scare extends Move {
    
    public Scare() {
        super("Scare", "Make up to 2 creatures lose 1HP.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        for(Creature target:targets) target.gain(-1, s.health);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=2;
    }
}
