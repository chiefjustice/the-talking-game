package Combat.Moves.MageMoves.Level1;

import Combat.Battle;
import Combat.Creature;
import Combat.Enemy;
import Combat.Moves.Move;

public class Taunt extends Move {
    public Taunt() {
        super("Taunt", "Change the target of 1 enemy to you.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(targets[0] instanceof Enemy) {
            ((Enemy) targets[0]).setTarget(user);
        } else {
            throw new Exception("Target is not an enemy. You cannot affect who they attack.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
}
