package Combat.Moves.MageMoves.Level1;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

/** Level 1 */
public class Poison extends Move {

    public Poison() {
        super("Poison", "Apply 1 poison to 1 creature without poison.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(targets[0].get(s.poison)==0) {
            targets[0].gain(1, s.poison);
        } else {
            throw new Exception("Target is already poisoned.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
}
