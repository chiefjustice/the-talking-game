package Combat.Moves.MageMoves.Level5;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class ForceField extends Move {
    int uses;

    public ForceField() {
        super("ForceField", "(3 uses per battle) Apply 11 block to up to 2 creatures.");
    }

    @Override
    public void battleStart() {
        uses = 3;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            for(Creature current:targets) current.gain(11, s.block);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=2;
    }
    
}
