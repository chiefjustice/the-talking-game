package Combat.Moves.MageMoves.Level5;

import Combat.Battle;
import Combat.Creature;
import Combat.EnemyStats;
import Combat.Moves.Move;

public class ElridgeGrasp extends Move {

    public ElridgeGrasp() {
        super("ElridgeGrasp", "Deal 15 damage to 1 creature. Summon 5 enemy lesser arms.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        user.dealDamage(targets[0], 15);
        for(int i=0;i<5;i++) battle.add(EnemyStats.lesserArm());
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
    
}
