package Combat.Moves.MageMoves.Level5;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Fright extends Move {

    public Fright() {
        super("Terror", "Make up to 5 creatures lose 3HP.");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        for(Creature current:targets) {
            current.gain(-3, s.health);
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=5;
    }
    
}
