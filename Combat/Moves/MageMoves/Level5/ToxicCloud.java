package Combat.Moves.MageMoves.Level5;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class ToxicCloud extends Move {
    public ToxicCloud() {
        super("ToxicCloud", "Apply 1 poison to up to 4 creatures");
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        for(Creature current:targets) current.gain(1, s.poison);
    }

    @Override
    public boolean validTargetCount(int count) {
        return count<=4;
    }
    
}
