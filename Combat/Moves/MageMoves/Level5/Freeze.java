package Combat.Moves.MageMoves.Level5;

import Combat.Battle;
import Combat.Creature;
import Combat.Creature.s;
import Combat.Moves.Move;

public class Freeze extends Move {
    int uses;

    public Freeze() {
        super("Freeze", "(1 use per battle) Apply 2 stun to 1 creature.");
    }

    @Override
    public void battleStart() {
        uses=1;
    }

    @Override
    public void affect(Battle battle, Creature user, Creature... targets) throws Exception {
        if(uses>=1) {
            targets[0].gain(2, s.stun);
            uses--;
        } else {
            throw new Exception("Out of uses this battle.");
        }
    }

    @Override
    public boolean validTargetCount(int count) {
        return count==1;
    }
    
}
